package com.myntra.styledelistingtests;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.myntra.api.HTTPBody;
import com.myntra.api.HTTPResponseHandlers;
import com.myntra.data.Constants;
import com.myntra.styledelisting.dataprovider.DataProviderFactory;
import com.myntra.style_delisting.helper.StyleDelistingServiceCalls;
import com.myntra.style_delisting.utils.Utilities;
import com.myntra.styledelistingvalidator.StyleDelistingValidator;
import com.myntra.utils.test_utils.BaseTest;

public class StyleDelistingTest extends BaseTest {

	private static StyleDelistingServiceCalls serviceHelper;

	public StyleDelistingTest() {
		serviceHelper = new StyleDelistingServiceCalls();
	}

	@Test
	public void testApproveCentralDelisting() {

		try
		{
			String body = Utilities.replaceParams("request-data/request.json", false, "planType", Constants.plan_central_delisting, "team", Constants.role_cm,"businessUnit", "Men's Casual", "styleId", 12345);
			HTTPResponseHandlers handlers = serviceHelper.postCreatePlan(body);
			HTTPBody resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_cm_pending);
			
			int planid = resbody.path("$.data.[0].id");
			int id = resbody.path("$.data.[0].planDetailEntries.[0].id");
			body = Utilities.replaceParams("request-data/upsert.json", false, "id", id, "styleStatus", Constants.request_do_not_delist);
			handlers = serviceHelper.putBulkUpsert(body, planid);
			resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validateStyleStatus(resbody, Constants.job_status_success,  Constants.request_do_not_delist);
			
			body = Utilities.replaceParams("request-data/dotransition.json", false, "planId", planid, "role", Constants.role_cm, "delistingEvent", Constants.submit_plan);
			handlers = serviceHelper.postDoTransaction(body);
			resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_ct_pending);
			
	
			body = Utilities.replaceParams("request-data/upsert.json", false, "id", id, "styleStatus", Constants.request_accept);
			handlers = serviceHelper.putBulkUpsert(body, planid);
			resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validateStyleStatus(resbody, Constants.job_status_success,  Constants.request_accept);
	
			body = Utilities.replaceParams("request-data/dotransition.json", false, "planId", planid, "role", Constants.role_ct, "delistingEvent", Constants.approve_plan);
			handlers = serviceHelper.postDoTransaction(body);
			resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_status_completed);
			
		}catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail();
		}
		
	}
	
	
	
	@Test
	public void testRejectCentralDelistng()
	{
		try
		{
			String body = Utilities.replaceParams("request-data/request.json", false, "planType", Constants.plan_central_delisting, "businessUnit", "Men's Casual", "styleId", 12345, "storeId", 1);
			HTTPResponseHandlers handlers = serviceHelper.postCreatePlan(body);
			HTTPBody resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_cm_pending);
			
			int planid = resbody.path("$.data.[0].id");
			int id = resbody.path("$.data.[0].planDetailEntries.[0].id");
			body = Utilities.replaceParams("request-data/upsert.json", false, "id", id, "styleStatus", Constants.request_do_not_delist);
			handlers = serviceHelper.putBulkUpsert(body, planid);
			resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validateStyleStatus(resbody, Constants.job_status_success,  Constants.request_do_not_delist);
			
			body = Utilities.replaceParams("request-data/dotransition.json", false, "planId", planid, "role", Constants.role_cm, "delistingEvent", Constants.submit_plan);
			handlers = serviceHelper.postDoTransaction(body);
			resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_ct_pending);
			
	
			body = Utilities.replaceParams("request-data/upsert.json", false, "id", id, "styleStatus", Constants.request_reject);
			handlers = serviceHelper.putBulkUpsert(body, planid);
			resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validateStyleStatus(resbody, Constants.job_status_success,  Constants.request_reject);
	
			body = Utilities.replaceParams("request-data/dotransition.json", false, "planId", planid, "role", Constants.role_ct, "delistingEvent", Constants.approve_plan);
			handlers = serviceHelper.postDoTransaction(body);
			resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_status_completed);

			
		}catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail();
		}
		
	}
	
	
	@Test
	public void testApproveWithNoRequest()
	{
		String body = Utilities.replaceParams("request-data/request.json", false, "planType", Constants.plan_central_delisting, "businessUnit", "Men's Casual", "styleId", 12345, "storeId", 1);
		HTTPResponseHandlers handlers = serviceHelper.postCreatePlan(body);
		HTTPBody resbody = handlers.getHTTPResponse().getBody();
		StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_cm_pending);
		
		int planid = resbody.path("$.data.[0].id");
		body = Utilities.replaceParams("request-data/dotransition.json", false, "planId", planid, "role", Constants.role_cm, "delistingEvent", Constants.submit_plan);
		handlers = serviceHelper.postDoTransaction(body);
		resbody = handlers.getHTTPResponse().getBody();
		StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_status_completed);
	}
	
	@Test
	public void delistByLoadingFile()
	{
		
	}

	
	
	@Test
	public void testManualDelisting()
	{
		try
		{
			String body = Utilities.replaceParams("request-data/request.json", false, "planType", Constants.plan_manual_permanent, "businessUnit", "Men's Casual","styleId", 12345, "storeId", 1);
			HTTPResponseHandlers handlers = serviceHelper.postCreatePlan(body);
			HTTPBody resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_status_completed);
	//		StyleDelistingValidator.validatePlanType(resbody, Constants.plan_manual_permanent);
		}catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail();
		}
		
	}
	
	
	@Test(dataProvider = "GetPlans", dataProviderClass = DataProviderFactory.class)
	public void testGetAllPlans(String plantype, String value)
	{
		try
		{
			HTTPResponseHandlers handlers = serviceHelper.getPlans(plantype, value);
			HTTPBody resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validateValuesInTheList(resbody, plantype, value);
			
		}catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	
	@Test
	public void testmasterPlansTypes()
	{
		try
		{
			HTTPResponseHandlers handlers = serviceHelper.getMasterPlans(null);
			HTTPBody resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validateMasterPlanType(resbody);
		}catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	
	@Test
	public void testmasterPlansStatus()
	{
		try
		{
			HTTPResponseHandlers handlers = serviceHelper.getMasterPlanStatus(null);
			HTTPBody resbody = handlers.getHTTPResponse().getBody();
			StyleDelistingValidator.validateMasterPlanStatus(resbody);
		}catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail();
		}
	}
	

}
