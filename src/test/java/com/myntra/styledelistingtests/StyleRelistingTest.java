package com.myntra.styledelistingtests;

import org.testng.annotations.Test;

import com.myntra.api.HTTPBody;
import com.myntra.api.HTTPResponseHandlers;
import com.myntra.data.Constants;
import com.myntra.style_delisting.helper.StyleDelistingServiceCalls;
import com.myntra.style_delisting.utils.Utilities;
import com.myntra.styledelistingvalidator.StyleDelistingValidator;

public class StyleRelistingTest {
	
	private static StyleDelistingServiceCalls serviceHelper;

	public StyleRelistingTest() {
		serviceHelper = new StyleDelistingServiceCalls();
	}
	
	@Test
	public void relistStyles()
	{
		String body = Utilities.replaceParams("request-data/request.json", false, "planType", Constants.plan_relisting, "team", Constants.role_cm, "businessUnit", "Men's Casual", "styleId", "12345");
		HTTPResponseHandlers handlers = serviceHelper.postCreatePlan(body);
		HTTPBody resbody = handlers.getHTTPResponse().getBody();
		StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_ct_pending);
		
		int planid = resbody.path("$.data.[0].id");
		int id = resbody.path("$.data.[0].planDetailEntries.[0].id");
		body = Utilities.replaceParams("request-data/upsert.json", false, "id", id, "styleStatus", Constants.request_do_not_delist);
		handlers = serviceHelper.putBulkUpsert(body, planid);
		resbody = handlers.getHTTPResponse().getBody();
		StyleDelistingValidator.validateStyleStatus(resbody, Constants.job_status_success,  Constants.request_do_not_delist);
		
		body = Utilities.replaceParams("request-data/dotransition.json", false, "planId", planid, "role", Constants.role_ct, "delistingEvent", Constants.approve_plan);
		handlers = serviceHelper.postDoTransaction(body);
		resbody = handlers.getHTTPResponse().getBody();
		StyleDelistingValidator.validatePlanStaus(resbody, Constants.job_status_success, Constants.plan_status_completed);

}
}
