package com.myntra.styledelisting.dataprovider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.DataProvider;

public class DataProviderFactory {

	/**
	 * 
	 * Data Provider method for Login Credentials for Jeeves UI
	 *
	 */
	@DataProvider(name = "GetPlans")
	public static Iterator<Object[]> GetPlans() {

		List<Object[]> credentials = new ArrayList<Object[]>();
		credentials.add(new Object[] {"planType", "CENTRAL_DELISTING"});
		credentials.add(new Object[] {"planStatus", "IN_PROGRESS"});
		return credentials.iterator();
	}
	
}

