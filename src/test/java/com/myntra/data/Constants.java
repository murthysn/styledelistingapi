package com.myntra.data;

public interface Constants {

	public static String plan_cm_pending = "CM_PENDING";
	public static String plan_ct_pending = "CT_PENDING";
	public static String plan_created = "CREATED";
	public static String plan_in_progress = "IN_PROGRESS";
	public static String plan_central_delisting =  "CENTRAL_DELISTING";
	public static String plan_manual_permanent =  "MANUAL_PERMANENT";
	public static String plan_relisting = "RELISTING";
	public static String request_do_not_delist =  "REQUESTED";
	public static String request_accept =  "ACCEPTED";
	public static String request_reject =  "REJECTED";
	public static String submit_plan = "SEND_FOR_APPROVAL";
	public static String approve_plan = "APPROVE_PLAN";
	public static String role_cm = "CM";
	public static String role_ct = "CT";
	public static String plan_creation_status = "Plan added successfully";
	public static String status_type = "Plan added successfully";
	public static String style_not_requested = "NOT_REQUESTED";
	public static String job_status_success = "SUCCESS";
	public static String plan_status_completed = "COMPLETED";
	
}
