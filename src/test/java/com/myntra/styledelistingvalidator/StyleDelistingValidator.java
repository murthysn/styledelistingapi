package com.myntra.styledelistingvalidator;

import org.testng.Assert;

import com.myntra.api.HTTPBody;
import com.myntra.data.Constants;

public class StyleDelistingValidator {

	public static void validateJobStatus(HTTPBody resbody, String status) {
		Assert.assertEquals(resbody.path("$.status.statusType"), status);
	}

	public static void validatePlanStaus(HTTPBody resbody, String status, String planstatus) {
		validateJobStatus(resbody, status);
		Assert.assertEquals(resbody.path("$.data.[0].planStatus"), planstatus);
	}

	public static void validateStyleStatus(HTTPBody resbody, String status, String stylestatus) {
		validateJobStatus(resbody, status);
		Assert.assertEquals(resbody.path("$.data.[0].styleStatus"), stylestatus);
	}

	public static void validate(HTTPBody resbody, HTTPBody planresult) {
		Assert.assertTrue(resbody.path("$.data.[0].id").equals(planresult.path("$.data.[0].planId")));
		Assert.assertTrue(resbody.path("$.data.[0].styleId").equals(planresult.path("$.data.[0].styleId")));
		Assert.assertTrue(resbody.path("$.data.[0].styleStatus").equals(planresult.path("$.data.[0].styleStatus")));
	}

	public static void validatePlanType(HTTPBody resbody, String value) {
		try {
			Assert.assertEquals(resbody.path(".data.[0].planType"), value);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	public static void validateValuesInTheList(HTTPBody resbody, String key, String value) {
		int n= ((java.util.List<?>) resbody.path("$.data")).size();
		int flag=0;
		for(int i=0; i<n;i++)
		{
			System.out.println(resbody.path("$.data.["+i+"]."+key+"").toString());
			if(!(resbody.path("$.data.["+i+"]."+key+"")).toString().equals(value))
			{
				flag++;
				break;
			}
		}
		if(flag==0)
		{
			Assert.assertTrue(true);
		}else
		{
			Assert.fail();
		}
	}
	
	
	
	
	public static void validateMasterPlanStatus(HTTPBody resbody)
	{
		int n=((java.util.List<?>) resbody.path("$.data")).size();
		for(int i=0; i<n;i++)
		{
			String label = resbody.path("$.data.["+i+"].label");
			String value = resbody.path("$.data.["+i+"].value");
			String type = resbody.path("$.data.["+i+"].type");
			
			if(label.equals(Constants.plan_created)||label.equals(Constants.plan_in_progress)||label.equals(Constants.plan_cm_pending)||label.equals(Constants.plan_ct_pending)||label.equals(Constants.plan_status_completed))
			{
				Assert.assertTrue(true);
			}else
			{Assert.fail();}
			if(value.equals(Constants.plan_cm_pending)||value.equals(Constants.plan_in_progress)||value.equals(Constants.plan_created)||value.equals(Constants.plan_ct_pending)||value.equals(Constants.plan_status_completed))
			{
				Assert.assertTrue(true);
			}else
			{Assert.fail();}
			}
		}
	
	
	
	public static void validateMasterPlanType(HTTPBody resbody)
	{
		int n=((java.util.List<?>) resbody.path("$.data")).size();
		for(int i=0; i<n;i++)
		{
			String label = resbody.path("$.data.["+i+"].label");
			String value = resbody.path("$.data.["+i+"].value");
			String type = resbody.path("$.data.["+i+"].type");
			
			if(label.equals(Constants.plan_central_delisting)||label.equals(Constants.plan_manual_permanent))
			{
				Assert.assertTrue(true);
			}else
			{Assert.fail();}
			if(value.equals(Constants.plan_central_delisting)||label.equals(Constants.plan_manual_permanent))
			{
				Assert.assertTrue(true);
			}else
			{Assert.fail();}
			}
		}
		
	}
