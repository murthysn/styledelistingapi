package com.myntra.style_delisting.utils;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.myntra.api.HTTPBody;

public class Utilities {

	/**
	 * Replaces params in file having placeholders in format `${param-name}`
	 * 
	 * @param file
	 * @param args
	 * @return {@link String}
	 */
	public static String replaceParams(String file, Object... args) {
		return replaceParams(file, true, args);
	}

	/**
	 * Replaces params in file having placeholders in format `${param-name}`
	 * 
	 * @param file
	 * @param maintainOrder
	 * @param args
	 * @return {@link String}
	 */
	public static String replaceParams(String file, Boolean maintainOrder, Object... args) {
		try {
			String body = IOUtils.toString(Utilities.getInputStream(file), "utf-8");
			if (maintainOrder) {
				body = String.format(body.replaceAll("\\$\\{.*?\\}", "%s"), args);
			} else {
				for (int i = 0; i < args.length - 1; i += 2) {
					body = body.replaceFirst("\\$\\{" + args[i] + "\\}", args[i + 1].toString());
				}
			}
			return body;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Create {@link InputStream} on file resource
	 * 
	 * @param file
	 * @return {@link InputStream}
	 */
	public static InputStream getInputStream(String file) {

		String _file = file.trim();
		if (_file.startsWith("/")) {
			try {
				return Utilities.class.getResourceAsStream(_file);
			} catch (Exception e) {
				return Utilities.class.getResourceAsStream(_file.substring(1));
			}
		} else {
			try {
				return Utilities.class.getResourceAsStream('/' + _file);
			} catch (Exception e) {
				return Utilities.class.getResourceAsStream(_file.substring(1));
			}
		}
	}

	}

