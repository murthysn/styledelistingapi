package com.myntra.style.delisting;

public enum ServiceType {

	STYLE_DELISTING("delisting");

	private String name;

	ServiceType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

}
