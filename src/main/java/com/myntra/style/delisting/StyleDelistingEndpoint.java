package com.myntra.style.delisting;

public interface StyleDelistingEndpoint {

	public static final String GET_PLANS = "/myntra-delisting-service/delisting/plan/search?q=";

	public static final String DO_TRANSACTION = "/myntra-delisting-service/delisting/plan/transition";
	
	public static final String GET_MASTER_PLANS = "/myntra-delisting-service/delisting/master/search?q=type.eq:planType";
	
	public static final String GET_MASTER_PLAN_STATUS = "/myntra-delisting-service/delisting/master/search?q=type.eq:planStatus";
	
	public static final String GET_PLAN_STATUS = "/myntra-delisting-service/delisting/plan-detail/";
	
	public static final String PUT_BULK_UPSERT = "/myntra-delisting-service/delisting/plan-detail/bulkUpsert/";
	
	public static final String POST_DOTRANSACTION = "/myntra-delisting-service/delisting/plan/transition";
	
	public static final String POST_CREATEPLAN = "/myntra-delisting-service/delisting/plan/";
	
	public static final String PUT_DOWNLOADSTYLES = "/myntra-delisting-service/delisting/plan-detail/downloadStyles";
	
	public static final String PUT_UPDATEPLAN = "/myntra-delisting-service/delisting/plan/5";
	
	public static final String PUT_RUNCENTRALALGO = "/myntra-delisting-service/delisting/plan/runCentralAlgo";
	
	public static final String TEXTFILEUPLOAD = "myntra-delisting-service/delisting/plan/upload";
	
	public static final String GET_PLAN_DETAILS = "/myntra-delisting-service/delisting/plan-detail/search?q=planId.eq:";
	

}
