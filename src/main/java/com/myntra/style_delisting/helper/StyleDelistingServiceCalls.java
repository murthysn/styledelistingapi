package com.myntra.style_delisting.helper;

import org.eclipse.persistence.sessions.serializers.JSONSerializer;
import org.json.JSONObject;

import com.myntra.api.HTTPHeaders;
import com.myntra.api.HTTPQueryParams;
import com.myntra.api.HTTPRequestSpecification;
import com.myntra.api.HTTPResponseHandlers;
import com.myntra.style.delisting.ServiceType;
import com.myntra.style.delisting.StyleDelistingEndpoint;
import com.myntra.utils.test_utils.ServicesHelper;

public class StyleDelistingServiceCalls extends ServicesHelper {

	private final HTTPHeaders headers = new HTTPHeaders();

	public StyleDelistingServiceCalls() {
		headers.addHeader("Authorization", "Basic YTpi");
		headers.addHeader("Accept", "application/json");
		headers.addHeader("Content-Type", "application/json");
	}
	
	public HTTPResponseHandlers getPlans(String searchcriteria, String value) {
		HTTPResponseHandlers result = null;
		String query=null;
		try
		{
		HTTPQueryParams params = new HTTPQueryParams();

		HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, query);

		result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.GET_PLANS+searchcriteria+".eq:"+value).get();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	public HTTPResponseHandlers getMasterPlans(String body) {
		HTTPResponseHandlers result = null;
		try {
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, body);

			result =  getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification,StyleDelistingEndpoint.GET_MASTER_PLANS).get();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	public HTTPResponseHandlers getMasterPlanStatus(String body)
	{
		HTTPResponseHandlers result = null;
		try
		{
			HTTPQueryParams params = new HTTPQueryParams();
			
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, body);
			
			result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.GET_MASTER_PLAN_STATUS).get();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	public HTTPResponseHandlers getPlanDetails(int planid)
	{
		HTTPResponseHandlers result = null;
		try
		{
			
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers);
			
			result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.GET_PLAN_DETAILS+planid).get();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	public HTTPResponseHandlers putBulkUpsert(String body, int planid)
	{
		HTTPResponseHandlers result = null;
		try
		{
			HTTPQueryParams params = new HTTPQueryParams();
			
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, body);
			
			result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.PUT_BULK_UPSERT+planid).put();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	public HTTPResponseHandlers postDoTransaction(String body)
	{
		HTTPResponseHandlers result = null;
		try
		{
			HTTPQueryParams params = new HTTPQueryParams();
			
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, body);
			
			result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.DO_TRANSACTION).post();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	public HTTPResponseHandlers postCreatePlan(String body)
	{
		HTTPResponseHandlers result = null;
		try
		{
			HTTPQueryParams params = new HTTPQueryParams();
			
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, body);
			System.out.println(headers);
			result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.POST_CREATEPLAN).post();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	
	
	public HTTPResponseHandlers putDownloadStyles(String body)
	{
		HTTPResponseHandlers result = null;
		try
		{
			HTTPQueryParams params = new HTTPQueryParams();
			
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, body);
			
			result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.PUT_DOWNLOADSTYLES).get();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	public HTTPResponseHandlers putUpdatePlan(String body)
	{
		HTTPResponseHandlers result = null;
		try
		{
			HTTPQueryParams params = new HTTPQueryParams();
			
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, body);
			
			result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.PUT_UPDATEPLAN).get();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	public HTTPResponseHandlers postRunCentralAlgo(String body)
	{
		HTTPResponseHandlers result = null;
		try
		{
			HTTPQueryParams params = new HTTPQueryParams();
			
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, body);
			
			result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.PUT_RUNCENTRALALGO).post();
			
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	public HTTPResponseHandlers putFileTextUpload(String body)
	{
		HTTPResponseHandlers result = null;
		try
		{
			HTTPQueryParams params = new HTTPQueryParams();
			
			HTTPRequestSpecification specification = new HTTPRequestSpecification(headers, body);
			
			result = getRestClient().createRequest(ServiceType.STYLE_DELISTING.toString(), specification, StyleDelistingEndpoint.TEXTFILEUPLOAD).post();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	
}
